# The Lay of Asgard

Herein the sisters describe the lay of Asgard. They solve their problems once, set simple boundaries, and build lasting foundations. Using a [simple philosophy](Philosophy.md), we create and document the simplest infrastructure that we claim is reasonably secure.

```mermaid
  graph LR
    OnlinePki[Online PKI] --> OfflinePki[Offline PKI]
    Vault --> OnlinePki
    Consul --> OnlinePki
    Nomad --> OnlinePki
    Nomad --> NomadVault

    NomadVault[Nomad-Vault Integration] --> Vault

    Database[Postgres] --> OnlinePki
    SecurityAutomatedDatabase[Postgres-Vault Integration] --> Database
    SecurityAutomatedDatabase --> Vault

    MessageQueue[RabbitMQ] --> OnlinePki
    SecurityAutomatedMessageQueue[RabbitMQ-Vault Integration] --> MessageQueue
    SecurityAutomatedMessageQueue --> Vault

    click Consul "https://gitlab.com/adriennes-spells/consul-google"
    click Vault "https://gitlab.com/adriennes-spells/raft-vault-pki-google"
    click Nomad "https://gitlab.com/adriennes-spells/nomad-google"
```
