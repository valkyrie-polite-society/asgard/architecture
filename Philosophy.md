# The Lay of Asgard

The Shield Sisters of Asgard set forth their views as follows: 

## Use Terraform to provision infrastructure
## Use DAG to coordinate module dependencies

Our brethren sometimes find difficulty in two modules that they wish to house in one Terraform state. This often occurs when they wish to both create and run Terraform code against the same service. This requires working aroud Terraform's expectation that provider configuration inputs are known at plan time. Therefore, we separate the configurations to satisfy Terraform's expectation and use directed, acyclic graphs to reason about and coordinate module interdependencies. We reject designs that cannot be expressed as a DAG.

## Secrets may reside in Terraform remote state

Secrets and code are the building materials of infrastructure, and they will live in our provisioning tools one way or the other. For example, Ansible has vaults, and Chef has encrypted databags. Terraform resources may store their values in remote state, and we are not worried about that. We would rather to use the Vault provider than build complicated, fragile processes solely to keep a secret out of Terraform state.

## Use datastores with Vault integrations

Postgres is a better database than FoundationDB, because you don't write a Vault secrets engine just to secure it. RabbitMQ is a better choice than Kafka because the integration is right there. If you want to use a datastore without a Vault integration you should build one yourself or reconsider using it.
